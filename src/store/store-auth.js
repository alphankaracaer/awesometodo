import { LocalStorage, Loading } from "quasar";
import { firebaseAuth } from "boot/firebase";
import { showErrorMessage } from "src/functions/function-show-error-message";

// import store in index.js file
const state = {
  loggedIn: false
};

const mutations = {
  setLoggedIn(state, value) {
    state.loggedIn = value;
  }
};

// To trigger an action within an action, we need to use dispatch method
const actions = {
  registerUser({ }, payload) {
    Loading.show();
    firebaseAuth
      .createUserWithEmailAndPassword(payload.email, payload.password)
      .then(response => {
        console.log("response: ", response);
      })
      .catch(error => {
        showErrorMessage(error.message);
      });
  },
  loginUser({ }, payload) {
    Loading.show();
    firebaseAuth
      .signInWithEmailAndPassword(payload.email, payload.password)
      .then(response => {
        console.log("response: ", response);
      })
      .catch(error => {
        showErrorMessage(error.message);
      });
  },
  logoutUser() {
    firebaseAuth.signOut();
  },
  handleAuthStateChange({ commit, dispatch }) {
    // this function is fired by firebase api when the use logs in or out
    firebaseAuth.onAuthStateChanged(user => {
      Loading.hide();
      if (user) {
        commit("setLoggedIn", true);
        LocalStorage.set("loggedIn", true);
        this.$router.push("/").catch(err => {
          console.log("Navigation Duplicated: ", user);
        });
        // To dispatch an action from a different module, add module name and slash before action
        dispatch('tasks/fbReadData', null, { root: true });
      } else {
        commit("tasks/clearTasks", null, { root: true });
        commit("setLoggedIn", false);
        commit("tasks/setTasksDownloaded", false, { root: true });
        LocalStorage.set("loggedIn", false);
        this.$router.replace("/auth").catch(err => {
          console.log("Navigation Duplicated... 1");
        });
      }
    });
  }
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
