import Vue from "vue";
import { uid, Notify } from "quasar";
import { firebaseDb, firebaseAuth } from "boot/firebase";
import { showErrorMessage } from "src/functions/function-show-error-message";

// import store in index.js file
const state = {
  tasks: {},
  search: "",
  sort: "name",
  tasksDownloaded: false
  // Another way of showing addTaskModal is from store,
  // add actions and mutations to change its value
  //
  // showAddTaskModal: false
};

const mutations = {
  updateTask(state, payload) {
    console.log("payload (from mutation): ", payload);
    Object.assign(state.tasks[payload.id], payload.updates);
  },
  deleteTask(state, id) {
    console.log("deleteTask: ", id);
    Vue.delete(state.tasks, id);
  },
  addTask(state, payload) {
    Vue.set(state.tasks, payload.id, payload.task);
  },
  clearTasks(stat) {
    state.tasks = {};
  },
  setSearch(state, value) {
    state.search = value;
  },
  setSort(state, value) {
    state.sort = value;
  },
  setTasksDownloaded(state, value) {
    state.tasksDownloaded = value;
  }
};

const actions = {
  updateTask({ dispatch }, payload) {
    console.log("payload: ", payload);
    dispatch("fbUpdateTask", payload);
  },
  deleteTask({ dispatch }, id) {
    dispatch("fbDeleteTask", id);
  },
  addTask({ dispatch }, task) {
    let taskId = uid();
    let payload = {
      id: taskId,
      task: task
    };
    dispatch("fbAddTask", payload);
  },
  setSearch({ commit }, value) {
    commit("setSearch", value);
  },
  setSort({ commit }, value) {
    commit("setSort", value);
  },
  fbReadData({ commit }) {
    let userId = firebaseAuth.currentUser.uid;
    let userTasks = firebaseDb.ref('tasks/' + userId);

    // initial check for data
    userTasks.once('value', snapshot => {
      commit('setTasksDownloaded', true);
    }, error => {
      if (error) {
        showErrorMessage(error.message);
        this.$router.replace("/auth");
      }
    });

    // child added
    userTasks.on('child_added', snapshot => {
      console.log('child_added');
      let task = snapshot.val();
      let payload = {
        id: snapshot.key,
        task: task
      };
      commit('addTask', payload);
    });

    // child changed
    userTasks.on('child_changed', snapshot => {
      console.log('child_changed');
      let task = snapshot.val();
      let payload = {
        id: snapshot.key,
        updates: task
      };
      commit('updateTask', payload);
    });

    // child removed
    userTasks.on('child_removed', snapshot => {
      console.log('child_removed');
      let taskId = snapshot.key;
      commit('deleteTask', taskId);
    });
  },
  fbAddTask({ }, payload) {
    let userId = firebaseAuth.currentUser.uid;
    let taskRef = firebaseDb.ref('tasks/' + userId + '/' + payload.id);
    taskRef.set(payload.task, error => {
      if (error) {
        showErrorMessage(error.message);
      }
      else {
        Notify.create("Task Updated!");
      }
    });
  },
  fbUpdateTask({ }, payload) {
    let userId = firebaseAuth.currentUser.uid;
    let taskRef = firebaseDb.ref('tasks/' + userId + '/' + payload.id);
    taskRef.update(payload.updates, error => {
      if (error) {
        showErrorMessage(error.message);
      }
      else {
        let keys = Object.keys(payload.updates);
        if (!(keys.includes("completed") && keys.length == 1)) {
          Notify.create("Task Added!");
        }
      }
    });

  },
  fbDeleteTask({ }, taskId) {
    let userId = firebaseAuth.currentUser.uid;
    let taskRef = firebaseDb.ref('tasks/' + userId + '/' + taskId);
    taskRef.remove(error => {
      if (error) {
        showErrorMessage(error.message);
      }
      else {
        Notify.create("Task Deleted!");
      }
    });
  }
};

const getters = {
  tasksSorted: state => {
    let tasksSorted = {};
    let keysOrdered = Object.keys(state.tasks);

    keysOrdered.sort((a, b) => {
      let taskAProp = state.tasks[a][state.sort].toLowerCase();
      let taskBProp = state.tasks[b][state.sort].toLowerCase();

      if (taskAProp > taskBProp) {
        return 1;
      } else if (taskAProp < taskBProp) {
        return -1;
      } else {
        return 0;
      }
    });
    keysOrdered.forEach(key => {
      tasksSorted[key] = state.tasks[key];
    });
    return tasksSorted;
  },
  tasksFiltered: (state, getters) => {
    let tasksSorted = getters.tasksSorted;
    let tasksFiltered = {};
    if (state.search) {
      Object.keys(tasksSorted).forEach(function (key) {
        let task = tasksSorted[key];
        let taskNameLowerCase = task.name.toLowerCase();
        let searchLowerCase = state.search.toLowerCase();
        if (taskNameLowerCase.includes(searchLowerCase)) {
          tasksFiltered[key] = task;
        }
      });
      return tasksFiltered;
    }
    return tasksSorted;
  },
  tasksTodo: (state, getters) => {
    let tasksFiltered = getters.tasksFiltered;
    let tasks = {};
    Object.keys(tasksFiltered).forEach(function (key) {
      let task = tasksFiltered[key];
      if (!task.completed) {
        tasks[key] = task;
      }
    });
    return tasks;
  },
  tasksCompleted: (state, getters) => {
    let tasksFiltered = getters.tasksFiltered;
    let tasks = {};
    Object.keys(tasksFiltered).forEach(function (key) {
      let task = tasksFiltered[key];
      if (task.completed) {
        tasks[key] = task;
      }
    });
    return tasks;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
