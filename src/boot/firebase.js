// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
//import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/database"
import "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyA_eq_Qyb4HtjLEDprxELnUT9Ws5bjIAVg",
  authDomain: "awesome-todo-789f8.firebaseapp.com",
  databaseURL: "https://awesome-todo-789f8.firebaseio.com",
  projectId: "awesome-todo-789f8",
  storageBucket: "awesome-todo-789f8.appspot.com",
  messagingSenderId: "236305617979",
  appId: "1:236305617979:web:8b250ccadbacc2d3ac781e",
  measurementId: "G-3V1Y2XRCXP"
};
// Initialize Firebase
let firebaseApp = firebase.initializeApp(firebaseConfig);
//firebase.analytics();
let firebaseAuth = firebase.auth();
let firebaseDb = firebase.database();

export { firebaseAuth, firebaseDb }